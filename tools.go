// Tools to simplify logging.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package log

import (
	"sync"
	"sync/atomic"
)

// logWith appends data to all log messages.
type logWith struct {
	format string        // The format string
	vs     []interface{} // The arguments
	l      Interface     // The underlying logger
}

// logPrefixWith prefixes data to all log messages.
type logPrefixWith struct {
	format string        // The format string
	vs     []interface{} // The arguments
	l      Interface     // The underlying logger
}

// SwapLogger wraps a logger that may be safely replaced while other go routines use the SwapLogger concurrently. The zero value for a SwapLogger will discard all log events without error.
type SwapLogger struct {
	logger atomic.Value
}

// BasicLogable provides an embeddable implementation of a Logable.
type BasicLogable struct {
	l SwapLogger
}

// loggerStruct is used by SwapLogger to encapsulate a logger.
type loggerStruct struct {
	Interface
}

// loggerLog wraps a Logger to provide a logger.
type loggerLog struct {
	l Logger
}

// ToMany combines multiple loggers, sending all log messages to each of loggers. ToMany is safe to be used concurrently. The zero value for a ToMany will discard all log events without an error.
type ToMany struct {
	m       sync.RWMutex // Mutex protecting the slice of loggers
	loggers []Interface  // The slice of loggers
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isLoggingToDiscard returns true if the ultimate logger is recognised as nil or Discard.
func isLoggingToDiscard(l Interface) bool {
	if l == nil || l == Discard {
		return true
	} else if lg, ok := l.(*logWith); ok {
		return isLoggingToDiscard(lg.l)
	} else if lg, ok := l.(*logPrefixWith); ok {
		return isLoggingToDiscard(lg.l)
	} else if lg, ok := l.(*SwapLogger); ok {
		return isLoggingToDiscard(lg.Unwrap())
	} else if lg, ok := l.(*ToMany); ok {
		return lg.isDiscard()
	} else if lg, ok := l.(*loggerLog); ok {
		return isLoggingToDiscard(lg.l.Log())
	}
	return false
}

/////////////////////////////////////////////////////////////////////////
// logWith functions
/////////////////////////////////////////////////////////////////////////

// With returns a logger, wrapping the given logger l, that automatically appends the given data to all log messages.
func With(l Interface, format string, v ...interface{}) Interface {
	// Is there anything to do?
	if l == nil || l == Discard {
		return Discard
	} else if len(format) == 0 {
		return l
	}
	// If l is a logWith logger, we copy over the data
	if lg, ok := l.(*logWith); ok {
		vs := make([]interface{}, len(v)+len(lg.vs))
		copy(vs, v)
		copy(vs[len(v):], lg.vs)
		return &logWith{
			format: format + " " + lg.format,
			vs:     vs,
			l:      lg.l,
		}
	}
	// Wrap the logger l in a new logWith logger
	vs := make([]interface{}, len(v))
	copy(vs, v)
	return &logWith{
		format: format,
		vs:     vs,
		l:      l,
	}
}

// Close calls Close on the underlying logger, if the underlying logger satisfies the io.Closer interface. Otherwise it does nothing.
func (l *logWith) Close() error {
	return closeLog(l.l)
}

// Printf logs the given message.
func (l *logWith) Printf(format string, v ...interface{}) {
	// Is there anything to do?
	lg := unwrap(l.l)
	if isLoggingToDiscard(lg) {
		return
	}
	// Create the format string and arguments
	if len(format) == 0 {
		format = l.format
	} else {
		format += " " + l.format
	}
	vs := make([]interface{}, len(v)+len(l.vs))
	copy(vs, v)
	copy(vs[len(v):], l.vs)
	// Pass the data along to the underlying logger
	lg.Printf(format, vs...)
}

/////////////////////////////////////////////////////////////////////////
// logPrefixWith functions
/////////////////////////////////////////////////////////////////////////

// PrefixWith returns a logger, wrapping the given logger l, that automatically prepends the given data to all log messages.
func PrefixWith(l Interface, format string, v ...interface{}) Interface {
	// Is there anything to do?
	if l == nil || l == Discard {
		return Discard
	} else if len(format) == 0 {
		return l
	}
	// If l is a logPrefixWith logger, we copy over the data
	if lg, ok := l.(*logPrefixWith); ok {
		vs := make([]interface{}, len(lg.vs)+len(v))
		copy(vs, lg.vs)
		copy(vs[len(lg.vs):], v)
		return &logPrefixWith{
			format: lg.format + " " + format,
			vs:     vs,
			l:      lg.l,
		}
	}
	// Wrap the logger in a new logPrefixWith logger
	vs := make([]interface{}, len(v))
	copy(vs, v)
	return &logPrefixWith{
		format: format,
		vs:     vs,
		l:      l,
	}
}

// Close calls Close on the underlying logger, if the underlying logger satisfies the io.Closer interface. Otherwise it does nothing.
func (l *logPrefixWith) Close() error {
	return closeLog(l.l)
}

// Printf logs the given message.
func (l *logPrefixWith) Printf(format string, v ...interface{}) {
	// Is there anything to do?
	lg := unwrap(l.l)
	if isLoggingToDiscard(lg) {
		return
	}
	// Create the format string and arguments
	if len(format) == 0 {
		format = l.format
	} else {
		format = l.format + " " + format
	}
	vs := make([]interface{}, len(l.vs)+len(v))
	copy(vs, l.vs)
	copy(vs[len(l.vs):], v)
	// Pass the data along to the underlying logger
	lg.Printf(format, vs...)
}

/////////////////////////////////////////////////////////////////////////
// SwapLogger functions
/////////////////////////////////////////////////////////////////////////

// unwrap checks whether the given logger is a SwapLogger and, if so, recursively calls unwrap until the first non-SwapLogger is reached.
func unwrap(l Interface) Interface {
	if l == nil || l == Discard {
		return Discard
	} else if sl, ok := l.(*SwapLogger); ok {
		return unwrap(sl.Unwrap())
	}
	return l
}

// Close calls Close on the wrapped logger, if the wrapped logger satisfies the io.Closer interface. Otherwise it does nothing.
func (l *SwapLogger) Close() error {
	return closeLog(l.Unwrap())
}

// Print logs the given data to the wrapped logger. It does not log anything if the wrapped logger is nil.
func (l *SwapLogger) Print(v ...interface{}) {
	print(l.Unwrap(), v...)
}

// Printf logs the given message to the wrapped logger. It does not log anything if the wrapped logger is nil.
func (l *SwapLogger) Printf(format string, v ...interface{}) {
	lg := unwrap(l.Unwrap())
	if isLoggingToDiscard(lg) {
		return
	}
	lg.Printf(format, v...)
}

// Println logs the given data to the wrapped logger. It does not log anything if the wrapped logger is nil.
func (l *SwapLogger) Println(v ...interface{}) {
	println(l.Unwrap(), v...)
}

// Unwrap returns the wrapped logger. If the wrapped logger is nil, then Discard is returned.
func (l *SwapLogger) Unwrap() Interface {
	if l == nil {
		return Discard
	}
	s, ok := l.logger.Load().(loggerStruct)
	if !ok {
		return Discard
	}
	return s.Interface
}

// Swap replaces the currently wrapped logger with lg. This will panic if l is nil.
func (l *SwapLogger) Swap(lg Interface) {
	l.logger.Store(loggerStruct{lg})
}

/////////////////////////////////////////////////////////////////////////
// BasicLogable
/////////////////////////////////////////////////////////////////////////

// SetLogger sets the logger.
func (b *BasicLogable) SetLogger(l Interface) {
	if b != nil {
		b.l.Swap(l)
	}
}

// Log returns the logger.
func (b *BasicLogable) Log() Interface {
	if b == nil {
		return Discard
	}
	return &b.l
}

/////////////////////////////////////////////////////////////////////////
// loggerLog functions
/////////////////////////////////////////////////////////////////////////

// ToLog allows logging directly to the given Logger.
func ToLog(l Logger) Interface {
	if l == nil {
		return Discard
	}
	return &loggerLog{l: l}
}

// Print logs the given data to the wrapped Logger.
func (l *loggerLog) Print(v ...interface{}) {
	print(l.l.Log(), v...)
}

// Printf logs the given message to the wrapped Logger.
func (l *loggerLog) Printf(format string, v ...interface{}) {
	lg := unwrap(l.l.Log())
	if isLoggingToDiscard(lg) {
		return
	}
	lg.Printf(format, v...)
}

// Println logs the given data to the wrapped Logger.
func (l *loggerLog) Println(v ...interface{}) {
	println(l.l.Log(), v...)
}

/////////////////////////////////////////////////////////////////////////
// ToMany functions
/////////////////////////////////////////////////////////////////////////

// isDiscard returns true if it can be determined that all log messages sent to this logger will be discarded.
func (l *ToMany) isDiscard() bool {
	if l != nil {
		l.m.RLock()
		for _, lg := range l.loggers {
			if !isLoggingToDiscard(lg) {
				l.m.RUnlock()
				return false
			}
		}
		l.m.RUnlock()
	}
	return true
}

// Len returns the number of loggers being logged to.
func (l *ToMany) Len() (n int) {
	if l != nil {
		l.m.RLock()
		n = len(l.loggers)
		l.m.RUnlock()
	}
	return
}

// Loggers returns the slice of loggers being logged to.
func (l *ToMany) Loggers() (loggers []Interface) {
	if l != nil {
		l.m.RLock()
		loggers = make([]Interface, len(l.loggers))
		copy(loggers, l.loggers)
		l.m.RUnlock()
	}
	if loggers == nil {
		loggers = []Interface{}
	}
	return
}

// Reset returns the current slice of loggers, and empties this slice.
func (l *ToMany) Reset() (loggers []Interface) {
	if l != nil {
		l.m.Lock()
		loggers = l.loggers
		l.loggers = nil
		l.m.Unlock()
	}
	if loggers == nil {
		loggers = []Interface{}
	}
	return
}

// Append appends the given loggers to the slice of loggers being logged to. This will panic if l is nil. Returns l in order to simplify usage: for example,
//	l := new(log.ToMany).Append(lg, log.Stderr)
func (l *ToMany) Append(lg ...Interface) *ToMany {
	if len(lg) != 0 {
		l.m.Lock()
		if l.loggers == nil {
			l.loggers = make([]Interface, 0, len(lg))
		}
		l.loggers = append(l.loggers, lg...)
		l.m.Unlock()
	}
	return l
}

// Close calls Close on each of the associated loggers, if the logger satisfies the io.Closer interface.
func (l *ToMany) Close() (err error) {
	if l != nil {
		l.m.RLock()
		for _, lg := range l.loggers {
			if closeErr := closeLog(lg); err == nil {
				err = closeErr
			}
		}
		l.m.RUnlock()
	}
	return
}

// Print logs the given data to the slice of loggers.
func (l *ToMany) Print(v ...interface{}) {
	if l != nil {
		l.m.RLock()
		for _, lg := range l.loggers {
			print(lg, v...)
		}
		l.m.RUnlock()
	}
}

// Printf logs the given message to the slice of loggers.
func (l *ToMany) Printf(format string, v ...interface{}) {
	if l != nil {
		l.m.RLock()
		for _, lg := range l.loggers {
			if !isLoggingToDiscard(lg) {
				lg.Printf(format, v...)
			}
		}
		l.m.RUnlock()
	}
}

// Println logs the given data to the slice of loggers.
func (l *ToMany) Println(v ...interface{}) {
	if l != nil {
		l.m.RLock()
		for _, lg := range l.loggers {
			println(lg, v...)
		}
		l.m.RUnlock()
	}
}
