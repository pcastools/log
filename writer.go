// Writer wraps an Interface as an io.Writer.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package log

import (
	"bytes"
	"strings"
)

// Writer wraps a logger and implements the io.Writer interface. Data written to the writer will be buffered until a new line '\n', at which point the buffered data will be passed to the underlying logger.
type Writer struct {
	L Interface    // The logger
	b bytes.Buffer // The buffer
}

/////////////////////////////////////////////////////////////////////////
// Writer functions
/////////////////////////////////////////////////////////////////////////

// Len returns the number of bytes in the buffer.
func (w *Writer) Len() int {
	if w == nil {
		return 0
	}
	return w.b.Len()
}

// Flush writes any buffered bytes to the underlying logger. The returned error is always nil, but is included to match bufio.Writer's Flush.
func (w *Writer) Flush() error {
	if w.Len() > 0 {
		println(w.L, w.b.String())
	}
	return nil
}

// Write appends the contents of p to the buffer, growing the buffer as needed. The return value n is the length of p; err is always nil. If the buffer becomes too large, Write will panic with ErrTooLarge. Buffered bytes will be written to the underlying logger upon encountering a new line '\n'.
func (w *Writer) Write(p []byte) (n int, err error) {
	// Can we send this directly to the logger?
	if w.Len() == 0 {
		idx := bytes.IndexByte(p, '\n')
		for idx != -1 {
			println(w.L, string(p[:idx]))
			p = p[idx+1:]
			n += idx + 1
			idx = bytes.IndexByte(p, '\n')
		}
	}
	// Is there anything left to do?
	if len(p) == 0 {
		return
	}
	// Can we empty the buffer?
	if idx := bytes.IndexByte(p, '\n'); idx != -1 {
		println(w.L, w.b.String()+string(p[:idx]))
		p = p[idx+1:]
		n += idx + 1
		// Can we send the remainder directly to the logger?
		idx = bytes.IndexByte(p, '\n')
		for idx != -1 {
			println(w.L, string(p[:idx]))
			p = p[idx+1:]
			n += idx + 1
			idx = bytes.IndexByte(p, '\n')
		}
		// Is there anything left to do?
		if len(p) == 0 {
			return
		}
	}
	// Write p to the buffer
	var m int
	m, err = w.b.Write(p)
	n += m
	return
}

// WriteByte appends the byte c to the buffer, growing the buffer as needed. The returned error is always nil, but is included to match bufio.Writer's WriteByte. If the buffer becomes too large, WriteByte will panic with ErrTooLarge. Buffered bytes will be written to the underlying logger upon encountering a new line '\n'.
func (w *Writer) WriteByte(c byte) error {
	if c == '\n' {
		println(w.L, w.b.String())
		return nil
	}
	return w.b.WriteByte(c)
}

// WriteRune appends the UTF-8 encoding of Unicode code point r to the buffer, returning its length and an error, which is always nil but is included to match bufio.Writer's WriteRune. The buffer is grown as needed; if it becomes too large, WriteRune will panic with ErrTooLarge. Buffered bytes will be written to the underlying logger upon encountering a new line '\n'.
func (w *Writer) WriteRune(r rune) (n int, err error) {
	if r == '\n' {
		println(w.L, w.b.String())
		n++
		return
	}
	n, err = w.b.WriteRune(r)
	return
}

// WriteString appends the contents of s to the buffer, growing the buffer as needed. The return value n is the length of s; err is always nil. If the buffer becomes too large, WriteString will panic with ErrTooLarge. Buffered bytes will be written to the underlying logger upon encountering a new line '\n'.
func (w *Writer) WriteString(s string) (n int, err error) {
	// Can we send this directly to the logger?
	if w.Len() == 0 {
		idx := strings.IndexByte(s, '\n')
		for idx != -1 {
			println(w.L, s[:idx])
			s = s[idx+1:]
			n += idx + 1
			idx = strings.IndexByte(s, '\n')
		}
	}
	// Is there anything left to do?
	if len(s) == 0 {
		return
	}
	// Can we empty the buffer?
	if idx := strings.IndexByte(s, '\n'); idx != -1 {
		println(w.L, w.b.String()+s[:idx])
		s = s[idx+1:]
		n += idx + 1
		// Can we send the remainder directly to the logger?
		idx = strings.IndexByte(s, '\n')
		for idx != -1 {
			println(w.L, s[:idx])
			s = s[idx+1:]
			n += idx + 1
			idx = strings.IndexByte(s, '\n')
		}
		// Is there anything left to do?
		if len(s) == 0 {
			return
		}
	}
	// Write s to the buffer
	var m int
	m, err = w.b.WriteString(s)
	n += m
	return
}
