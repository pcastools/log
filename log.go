// Log defines a lightweight logging interface, and utility functions for logging. Profoundly influenced by Dave Cheney's take on logging [https://dave.cheney.net/2015/11/05/lets-talk-about-logging].

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package log

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
)

// Interface describing a logging object. There is no guarantee that a call to Printf will result in the message being successfully logged; the contract with the user is simply that the logger will do "its best". You should be prepared for the possibility that some log messages are silently dropped.
type Interface interface {
	Printf(format string, v ...interface{})
}

// SetLoggerer is the interface satisfied by the SetLogger method.
type SetLoggerer interface {
	SetLogger(l Interface) // SetLogger sets a logger.
}

// Logger is the interface satisfied by the Log method.
type Logger interface {
	Log() Interface // Log returns the logger.
}

// Logable is the interface satisfied by the SetLogger and Log methods. It indicates an object that supports logging.
type Logable interface {
	SetLoggerer
	Logger
}

// printer is the interface satisfied by the Print method.
type printer interface {
	Print(v ...interface{})
}

// printlner is the interface satisfied by the Println method.
type printlner interface {
	Println(v ...interface{})
}

// jSONer is the interface satisfied by the JSON method.
type jSONer interface {
	JSON(data map[string]interface{}, format string, v ...interface{})
}

// logger contains the current global logger.
var logger = &SwapLogger{}

// A map (with controlling mutex m) to handle package-level debugging.
var (
	debugm   sync.RWMutex
	debugmap = map[string]bool{}
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// getPackageName tries to get the name of the current package. This is inferred from the source filename, and is determined to be everything up to but not including the last occurrence of '/'; if this fails for any reason then the empty string is returned.
func getPackageName() string {
	if _, file, _, ok := runtime.Caller(2); ok {
		return filepath.Dir(file)
	}
	return ""
}

// closeLog closes the logger, if it satisfies the io.Closer interface. Otherwise it does nothing.
func closeLog(l Interface) (err error) {
	if l = unwrap(l); l != Discard {
		if lg, ok := l.(io.Closer); ok {
			err = lg.Close()
		}
	}
	return
}

// print prints to the given logger. Arguments are handled in the manner of fmt.Print. It is permitted for the logger to silently drop log messages.
func print(l Interface, v ...interface{}) {
	if l = unwrap(l); isLoggingToDiscard(l) {
		return
	}
	if lg, ok := l.(printer); ok {
		lg.Print(v...)
		return
	}
	n := len(v)
	args := make([]string, 0, n)
	for i := 0; i < n; i++ {
		args = append(args, "%v")
	}
	l.Printf(strings.Join(args, " "), v...)
}

// println prints to the given logger. Arguments are handled in the manner of fmt.Println. It is permitted for the logger to silently drop log messages.
func println(l Interface, v ...interface{}) {
	if l = unwrap(l); isLoggingToDiscard(l) {
		return
	}
	if lg, ok := l.(printlner); ok {
		lg.Println(v...)
		return
	}
	n := len(v)
	args := make([]string, 0, n)
	for i := 0; i < n; i++ {
		args = append(args, "%v")
	}
	l.Printf(strings.Join(args, " ")+"\n", v...)
}

/////////////////////////////////////////////////////////////////////////
// Discard functions
/////////////////////////////////////////////////////////////////////////

// devNull discards all log messages.
type devNull int

// Discard is a logger that discards all log messages.
const Discard = devNull(0)

// Printf ignores all log messages.
func (devNull) Printf(_ string, _ ...interface{}) {}

/////////////////////////////////////////////////////////////////////////
// Stderr functions
/////////////////////////////////////////////////////////////////////////

// stderr logs all messages to os.Stderr
type stderr int

// Stderr logs all messages to os.Stderr
const Stderr = stderr(0)

func (stderr) Printf(format string, v ...interface{}) {
	if !strings.HasSuffix(format, "\n") {
		format += "\n"
	}
	fmt.Fprintf(os.Stderr, format, v...)
}

func (stderr) Print(v ...interface{}) {
	fmt.Fprint(os.Stderr, v...)
}

func (stderr) Println(v ...interface{}) {
	fmt.Fprintln(os.Stderr, v...)
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// SetLogger changes the global logger to l.
func SetLogger(l Interface) {
	if l == nil {
		l = Discard
	}
	logger.Swap(l)
}

// Log returns the current global logger.
func Log() Interface {
	return logger.Unwrap()
}

// Print prints to the global logger. Arguments are handled in the manner of fmt.Print. It is permitted for the logger to silently drop log messages.
func Print(v ...interface{}) {
	print(Log(), v...)
}

// Printf prints to the global logger. Arguments are handled in the manner of fmt.Printf. It is permitted for the logger to silently drop log messages.
func Printf(format string, v ...interface{}) {
	l := unwrap(Log())
	if isLoggingToDiscard(l) {
		return
	}
	l.Printf(format, v...)
}

// Println prints to the global logger. Arguments are handled in the manner of fmt.Println. It is permitted for the logger to silently drop log messages.
func Println(v ...interface{}) {
	println(Log(), v...)
}

// DebugPackage toggles debugging logging for the current package. A value of 'true' turns on debugging for the current package, and a value of 'false' turns off debugging for the current package. Calls to Debug() in a given package produce non-empty output only if debugging is turned on for that package. By default, debugging is turned off in every package. The current package name is inferred from the source filename, and determined to be everything up to but not including the last occurrence of '/'.
func DebugPackage(b bool) {
	packageName := getPackageName()
	debugm.Lock()
	debugmap[packageName] = b
	debugm.Unlock()
}

// Debug prints to the global logger, provided that debugging is turned on for this package (using DebugPackage). Arguments are handled in the manner of fmt.Print. It is permitted for the logger to silently drop log messages.
func Debug(v ...interface{}) {
	l := unwrap(Log())
	if isLoggingToDiscard(l) {
		return
	}
	packageName := getPackageName()
	debugm.RLock()
	ok := debugmap[packageName]
	debugm.RUnlock()
	if ok {
		print(l, v...)
	}
}

// Debugf prints to the global logger, provided that debugging is turned on for this package (using DebugPackage). Arguments are handled in the manner of fmt.Printf. It is permitted for the logger to silently drop log messages.
func Debugf(format string, v ...interface{}) {
	l := unwrap(Log())
	if isLoggingToDiscard(l) {
		return
	}
	packageName := getPackageName()
	debugm.RLock()
	ok := debugmap[packageName]
	debugm.RUnlock()
	if ok {
		l.Printf(format, v...)
	}
}

// Debugln prints to the global logger, provided that debugging is turned on for this package (using DebugPackage). Arguments are handled in the manner of fmt.Println. It is permitted for the logger to silently drop log messages.
func Debugln(v ...interface{}) {
	l := unwrap(Log())
	if isLoggingToDiscard(l) {
		return
	}
	packageName := getPackageName()
	debugm.RLock()
	ok := debugmap[packageName]
	debugm.RUnlock()
	if ok {
		println(l, v...)
	}
}

// JSON prints to the global logger. The first argument data will be marshalled into JSON format using the Marshal function in the standard encoding/json package. The last two arguments define a message to be logged alongside data, and are handled in the manner of fmt.Printf. It is permitted for the logger to silently drop log messages.
func JSON(data map[string]interface{}, format string, v ...interface{}) {
	l := unwrap(Log())
	if isLoggingToDiscard(l) {
		return
	}
	if lg, ok := l.(jSONer); ok {
		lg.JSON(data, format, v...)
		return
	}
	b, err := json.Marshal(data)
	if err != nil {
		b = []byte{} // Marshalling failed
	}
	format = "{\"message\":\"" + format + "\",\"data\":%s}"
	newv := make([]interface{}, len(v), len(v)+1)
	copy(newv, v)
	newv = append(newv, string(b))
	l.Printf(format, newv...)
}

// Close closes the global logger, if it satisfies the io.Closer interface. Otherwise it does nothing.
func Close() error {
	return closeLog(Log())
}
